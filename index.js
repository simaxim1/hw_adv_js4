/*

Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

Це технологія взаємодії із сервером у асінхроному режимі, яка дозволяє відправляти/отримувати запити у форматі XML та JSON.
Користь технології:
- динамічне оновлення сторінки без необхідності її перезвантаження;
- выдсутність необхідності очікування відповіді (сторінка залишається доступною для взаємодії користувача, без його блокування);
- можливість оновлення частково;
- можливість валідації данних без перевантаження;

*/

async function fetchData() {
    const response = await fetch("https://ajax.test-danit.com/api/swapi/films");
    const films = await response.json();
    const ul = document.createElement("ul");
  
    for (const film of films) {
      const li = document.createElement("li");
      const episodeId = document.createElement("p");

      episodeId.textContent = `Id of the episode: ${film.episodeId}`;
      li.appendChild(episodeId);
  
      const paragraph = document.createElement("p");
      paragraph.setAttribute("id", "name");
      paragraph.textContent = `Name of the film: ${film.name}`;
      li.appendChild(paragraph);
  
      const charactersList = document.createElement("ul");
      const charactersItem = document.createElement("p");
      charactersItem.textContent = "Characters of the film: ";
  
      const characterPromises = film.characters.map(characterUrl => fetch(characterUrl).then(res => res.json()));
      const characters = await Promise.all(characterPromises);
  
      for (const character of characters) {
        const liCharacterName = document.createElement("li");
        liCharacterName.textContent = character.name;
        charactersList.appendChild(liCharacterName);
      }
  
      li.appendChild(charactersItem);
      li.appendChild(charactersList);
  
      const openingCrawl = document.createElement("p");
      openingCrawl.textContent = `Opening crawl of the film: ${film.openingCrawl}`;

      li.appendChild(openingCrawl);
      ul.appendChild(li);
    }
  
    document.body.appendChild(ul);
}
  
fetchData();